﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : MonoBehaviour {

    [SerializeField] private GameManager gameManager;
    [SerializeField] private EasterEgg easterEgg;

    private bool operating = false;

    void Start() {
        easterEgg = FindObjectOfType<EasterEgg>();
    }

    void Update () {
        if (GameManager.isRunning() && Input.GetButtonDown("Fire1")) { easterEgg.RegisterMove(Vector2.zero, true); }
        if (GameManager.isRunning() && Input.GetButtonDown("Fire2")) { easterEgg.RegisterMove(Vector2.zero, false, true); }
        if (GameManager.isRunning() && Input.GetButtonDown("Xbox Start")) { easterEgg.RegisterMove(Vector2.zero, false, false, true); }

        if (GameManager.isRunning() && Input.GetButtonDown("Fire1") && 
            gameManager.CanInfect(gameManager.getTarget()) && 
            gameManager.getTarget().isOn() &&
            operating) {

            Computer target;

            if (gameManager.getNextTarget() != null) { target = gameManager.getNextTarget(); }
            else { target = gameManager.getTarget(); }

            target.SetComputerState(Computer.ComputerState.INFECTED);
        }
	}

    public void Begin()
    {
        operating = true;
    }
}
