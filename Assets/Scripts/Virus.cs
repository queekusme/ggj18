﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour {

    [SerializeField] private Computer computer;


    private float downloadedDataSize;

    private GameManager manager;

    // Use this for initialization
    void Start () {
        manager = FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (computer.GetComputerState() == Computer.ComputerState.INFECTED && computer.isOn() && GameManager.isRunning()) {
            downloadedDataSize += Time.deltaTime * manager.difficulty.downloadSpeed;
            if (downloadedDataSize > manager.difficulty.dataSize) { downloadedDataSize = manager.difficulty.dataSize; }
        }
	}

    public float GetMax() { return FindObjectOfType<GameManager>().difficulty.dataSize; }
    public float GetDownloaded() { return downloadedDataSize; }
}
