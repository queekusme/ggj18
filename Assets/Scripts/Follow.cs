﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class Follow : MonoBehaviour {

    private Camera m_camera;

    [SerializeField] private Vector3 cameraOffset;
    [SerializeField, Range(0.01f, 5.0f)] private float cameraMoveTime;

    [SerializeField] private Computer target;
    [SerializeField] private Computer nextTarget;

    [SerializeField] private MovementManager movementManager;

    private float lerpTime = 0;

    // Use this for initialization
    void Start () {
        m_camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        if (target == null) { return; }

        if (nextTarget == null) {
            transform.position = target.transform.position + cameraOffset;
        } else {
            lerpTime += Time.deltaTime;
            transform.position = Vector3.Lerp(target.transform.position, nextTarget.transform.position, lerpTime / cameraMoveTime) + cameraOffset;

            if (lerpTime >= cameraMoveTime) {
                lerpTime = 0;
                target = nextTarget;
                nextTarget = null;
                movementManager.Reset();
            }
        }
	}

    public void SetTarget(Computer _target, bool setTarget = false) {
        if (setTarget) { target = _target; return; }
        nextTarget = _target;
    }

    public Computer GetTarget() { return target; }
    public Computer GetNextTarget() { return nextTarget; }
}
