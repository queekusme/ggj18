﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antivirus : MonoBehaviour {

    [SerializeField] private Computer computer;

    [SerializeField, Range(0.1f, 10.0f)] private float disinfectTime;

    [SerializeField] private GameObject antivirusSymbol;
    [SerializeField, Range(0.0f, 1.0f)] private float minSquish;
    [SerializeField, Range(0.0f, 1.0f)] private float maxSquish;

    private float disinfectTimer = 0.0f;

    private bool operating = true;

	// Update is called once per frame
	void Update () {
        if (computer.GetComputerState() == Computer.ComputerState.INFECTED && computer.isOn() && operating) {
            disinfectTimer += Time.deltaTime;

            antivirusSymbol.transform.localScale =
                (Vector3.one * (
                    Remap(Mathf.Sin(Time.fixedTime * 5), -1.0f, 1.0f, minSquish, maxSquish) *
                    (1 - (disinfectTimer / disinfectTime))
                ));
        }

        if (disinfectTimer >= disinfectTime)
        {
            disinfectTimer = 0;
            computer.SetComputerState(Computer.ComputerState.NONE);
            computer.Reboot();
        }

	}

    public void setOperating(bool op){
        operating = op;
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
