﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EasterEgg : MonoBehaviour
{
    public static bool konami { get; private set; }

    private int step = 0;
    //uuddlrlrbas

    void Start() {
        if (FindObjectsOfType<EasterEgg>().Length > 1) { Destroy(gameObject); } else { konami = false; }
        DontDestroyOnLoad(this);
    }

    public void RegisterMove(Vector2 vector2, bool a = false, bool b = false, bool start = false)
    {
        switch (step)
        {
            case 0: if (vector2 == Vector2.up) { step++; } else { step = 0; } break;
            case 1: if (vector2 == Vector2.up) { step++; } else { step = 0; } break;
            case 2: if (vector2 == Vector2.down) { step++; } else { step = 0; } break;
            case 3: if (vector2 == Vector2.down) { step++; } else { step = 0; } break;
            case 4: if (vector2 == Vector2.left) { step++; } else { step = 0; } break;
            case 5: if (vector2 == Vector2.right) { step++; } else { step = 0; } break;
            case 6: if (vector2 == Vector2.left) { step++; } else { step = 0; } break;
            case 7: if (vector2 == Vector2.right) { step++; } else { step = 0; } break;
            case 8: if (b) { step++; } else { step = 0; } break;
            case 9: if (a) { step++; } else { step = 0; } break;
            case 10: if (start) { step++; } else { step = 0; } break;
        }
        if (step > 10) {
            // code complete;
            step = 0;
            //Do Something
            Debug.Log("Konami Code");
            konami = true;
        }
    }
}