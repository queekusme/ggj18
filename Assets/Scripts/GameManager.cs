﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    [SerializeField] public Difficulty difficulty;
    [SerializeField] private GameObject computerPrefab;
    [SerializeField, Range(1.0f, 5.0f)] private float spaceBetweenComputers;

    [SerializeField] private Follow cameraFollow;

    private Computer[,] network;

    [SerializeField] private UnityEngine.UI.Image extractionPercentBar;
    [SerializeField] private GameObject downloadComplete;
    [SerializeField] private GameObject completeButton;
    [SerializeField] private UnityEngine.EventSystems.EventSystem eventSystem;

    [SerializeField] private GameObject startScreen;
    [SerializeField] private ActionManager actionManager;
    [SerializeField] private MovementManager movementManager;
    [SerializeField] private GameObject score;
    [SerializeField] private GameObject winscreen;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private GameObject gameOverScreenReloadButton;
    [SerializeField] private GameObject dataWindow;
    [SerializeField] private GameObject winRestart;

    private static bool gamerunning = false;

    public int infectedCount{
        get {
            int count = 0;
            for (int x = 0; x < difficulty.networkSize.x; x++) {
                for (int y = 0; y < difficulty.networkSize.y; y++) {
                    count += (IsInfected(Get(new Vector2(x, y))) ? 1 : 0);  
                }
            }
            return count;
        }
    }

    // Use this for initialization
    public void StartGame() {
        network = new Computer[Mathf.FloorToInt(difficulty.networkSize.x), Mathf.FloorToInt(difficulty.networkSize.y)];

        for (int x = 0; x < difficulty.networkSize.x; x++) {
            for (int y = 0; y < difficulty.networkSize.y; y++) {
                GameObject computer_object = Instantiate(computerPrefab, new Vector3(x * spaceBetweenComputers, 0.0f, y * spaceBetweenComputers), Quaternion.Euler(0.0f, 180.0f, 0.0f), transform);
                Computer computer = computer_object.GetComponent<Computer>();
                computer.SetFocus(false);

                computer.SetID(new Vector2(x, y));
                network[x, y] = computer;
            }
        }

        Computer startingComp = network[Mathf.RoundToInt(Random.Range(0, difficulty.networkSize.x - 1.0f)), Mathf.RoundToInt(Random.Range(0, difficulty.networkSize.y - 1.0f))];
        startingComp.SetComputerState(Computer.ComputerState.INFECTED);

        cameraFollow.SetTarget(startingComp, true);
        startingComp.SetFocus(true);
        startingComp.GetComponent<Antivirus>().setOperating(false); // wait for first input

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Xbox Back")) { Application.Quit(); }

        score.SetActive(gamerunning);
        if (!isRunning()) { return; }

        extractionPercentBar.fillAmount = GetExtractionPercent();
        downloadComplete.SetActive(extractionPercentBar.fillAmount == 1.0f);
        if (downloadComplete.activeSelf) {
            eventSystem.SetSelectedGameObject(completeButton);
        }
        if(dataWindow.activeSelf)
        {
            eventSystem.SetSelectedGameObject(winRestart);
        }

        if (gamerunning && infectedCount == 0 && winscreen.activeSelf == false)
        {
            gameOverScreen.SetActive(true);
            eventSystem.SetSelectedGameObject(gameOverScreenReloadButton);
        }
    }

    public void Navigate(Vector2 moveBy) {

        Vector2 activeComputer = cameraFollow.GetTarget().GetID();
        cameraFollow.GetTarget().GetComponent<Antivirus>().setOperating(true); // wait for first input

        activeComputer = Offset(activeComputer, moveBy, difficulty.networkSize);

        Computer newComp = Get(activeComputer);

        if (newComp != cameraFollow.GetTarget()) {
            cameraFollow.GetTarget().SetFocus(false);
            newComp.SetFocus(true);
            cameraFollow.SetTarget(newComp);
        }
    }

    public Computer getTarget() { return cameraFollow.GetTarget(); }
    public Computer getNextTarget() { return cameraFollow.GetNextTarget(); }
    public Computer Get(Vector2 pos) {
        try {
            return network[Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y)];
        }catch(System.Exception e){
            return null;
        }
    }

    public static bool isRunning() { return gamerunning; }

    public bool IsInfected(Computer computer) {
        return computer != null && computer.GetComputerState() == Computer.ComputerState.INFECTED;
    }

    public bool CanInfect(Computer computer) {
        Vector2 compID = computer.GetID();

        if (IsInfected(Get(compID + Vector2.up))) { return true; }
        if (IsInfected(Get(compID + Vector2.down))) { return true; }
        if (IsInfected(Get(compID + Vector2.right))) { return true; }
        if (IsInfected(Get(compID + Vector2.left))) { return true; }

        return false;
    }

    public Vector2 Offset(Vector2 a, Vector2 b, Vector2 clamp) {
        a.x = Mod((a.x + b.x), clamp.x);
        a.y = Mod((a.y + b.y), clamp.y);
        return a;
    }

    private int Mod(float x, float m) {
        int xx = Mathf.RoundToInt(x);
        int mm = Mathf.RoundToInt(m);
        return (xx % mm + mm) % mm;
    }

    private float GetExtractionPercent()
    {
        float count = 0.0f;
        float max = 0.0f;

        for (int x = 0; x < difficulty.networkSize.x; x++)
        {
            for (int y = 0; y < difficulty.networkSize.y; y++)
            {
                Computer computer = Get(new Vector2(x, y));
                if (computer == null) { continue; }
                count += computer.GetComponent<Virus>().GetDownloaded();
                max += computer.GetComponent<Virus>().GetMax();
            }
        }

        return count / max;
    }

    public void ReloadScene()
    {
        gamerunning = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    public void SetDifficulty(Difficulty diff)
    {
        difficulty = diff;
    }
    
    public void BeginGame() {
        startScreen.SetActive(false);
        StartGame();
        actionManager.Begin();
        movementManager.Reset();
        gamerunning = true;
    }
}
