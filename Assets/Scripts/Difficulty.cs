﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Difficulty.asset", menuName = "Difficulty/New difficulty")]
public class Difficulty : ScriptableObject {
    public Vector2 networkSize;
    [Range(0.1f, 10.0f)] public float downloadSpeed; // e.g. 5mbps
    [Range(0.1f, 50.0f)] public float dataSize; // e.g. 5 GB
    [Range(0.0f, 1.0f)] public float randomRebootPercent; // e.g. 5 GB
    [Range(1.0f, 10.0f)] public float rebootTime;
}
