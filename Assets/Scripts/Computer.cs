﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Computer : MonoBehaviour {

    [SerializeField] private bool is_on;

    [SerializeField] private GameObject screen;

    [SerializeField] private Color noPower;
    [SerializeField] private Color hasPower;

    [SerializeField] private Vector2 id;

    public enum ComputerState { NONE, INFECTED };

    [SerializeField] private ComputerState computerState;
    [SerializeField] private GameObject infectedPrefab;
    [SerializeField] private Vector3 infectedOffset;

    [SerializeField] private GameObject focusIndicator;

    private GameObject infectedInstanciated;
    private GameManager manager;

    private Material ScreenMaterial { get { return screen.GetComponent<Renderer>().material; } }

    private void Start() {
        infectedInstanciated = Instantiate(infectedPrefab, transform.position + infectedOffset, Quaternion.identity, transform);
        manager = FindObjectOfType<GameManager>();
    }

    void Update () {
        ScreenMaterial.color = is_on ? hasPower : noPower;
        infectedInstanciated.SetActive(computerState == ComputerState.INFECTED);

        if (!EasterEgg.konami &&GameManager.isRunning() && Random.Range(0.0f, 1.0f) < manager.difficulty.randomRebootPercent && is_on)
        {
            Reboot();
        }
    }

    public void SetID(Vector2 id) { this.id = id; }
    public Vector2 GetID() { return this.id; }
    public void SetFocus(bool focus) { focusIndicator.SetActive(focus); }

    public bool isOn() { return is_on; }

    public void SetComputerState(ComputerState computerState) {
        this.computerState = computerState;
    }
    public ComputerState GetComputerState() { return this.computerState; }

    private IEnumerator _Reboot()
    {
        yield return new WaitForSeconds(manager.difficulty.rebootTime);
        is_on = true;
    }

    public void Reboot() {
        is_on = false;
        StartCoroutine(_Reboot());
    }
}
