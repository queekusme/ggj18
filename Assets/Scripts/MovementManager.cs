﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MovementManager : MonoBehaviour {

    [SerializeField] private GameManager gameManager;
    [SerializeField] private EasterEgg easterEgg;
    [SerializeField, Range(0.0f, 1.0f)] private float thumbstickDeadZone;

    AudioSource audio;

    bool flag = false;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        easterEgg = FindObjectOfType<EasterEgg>();
    }

    void Update () {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        Vector2 offset = new Vector2(Mathf.RoundToInt(horizontal), Mathf.RoundToInt(vertical));

        if (GameManager.isRunning()) {
            if ((Mathf.Abs(vertical) > thumbstickDeadZone || Mathf.Abs(horizontal) > thumbstickDeadZone)) {
                if (flag) {
                    gameManager.Navigate(offset);
                    easterEgg.RegisterMove(offset);
                    audio.Play();
                    flag = false;
                }
            }
        }

    }

    public void Reset()
    {
        flag = true;
    }
}
